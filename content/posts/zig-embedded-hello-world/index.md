+++
title = "Zig Embedded: Hello World !"
date = 2023-11-28T18:52:08+03:00
+++

[Zig](https://ziglang.org/) is an exciting new language, advertising itself as a *"a general-purpose programming language and toolchain for maintaining robust, optimal and reusable software."* Part of its charm comes from the fact that it's not as hard to learn as *Rust*, but still takes a saner approach to things like macros and `#define`s than C/C++. While searching to see if it can be used for embedded applications, I came across [microzig](https://github.com/ZigEmbeddedGroup/microzig). The frameworks is still in its infancy, but has pretty good support for [RP2040](https://www.raspberrypi.com/products/rp2040/), the litte famous chip made by Raspberry Pi, used in many hobbyist boards. Let's see if we can make it work for a "Hello World!" application!

# Requirements

## Zig

We obviously need `zig`, which you can get from the [downloads page](https://ziglang.org/download/). Microzig supports version `0.11.0`, so get that instead of `master`, and place it somewhere on your `$PATH`. Alternatively, you can use [zigup](https://github.com/marler8997/zigup) if you want to juggle multiple versions, but probably not needed in the beginning.

## RP2040

Next thing we need is

